# btssoap.rb - ruby interface for Debian BTS SOAP engine
# Copyright (C) 2006-2008  Junichi Uekawa <dancer@debian.org>
# Copyright (C) 2009-2022  Francesco Poli <invernomuto@paranoici.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License with
# the Debian GNU/Linux distribution in file /usr/share/common-licenses/GPL-2;
# if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

require 'soap/rpc/driver'
require 'aptlistbugs/debian/bug'

module Debian
  module BTSSOAP
    class StringArray < Array; end
    class Soap
      def initialize(host = "bugs.debian.org", port = 80)
        @server="http://#{host}:#{port}/cgi-bin/soap.cgi"
        @ns = 'Debbugs/SOAP/'
        @drv = SOAP::RPC::Driver.new(@server, @ns)
        @drv.wiredump_dev = $stderr if $DEBUG
        @drv.add_method('get_status','bugnumber')

        # the number of options should be variable, but I don't know how it should be done
        @drv.add_method('get_bugs','keyvalue')
        @drv.options["protocol.http.connect_timeout"] = 999
        @drv.options["protocol.http.send_timeout"] = 999
        @drv.options["protocol.http.receive_timeout"] = 999

        # define StringArray
        @map = SOAP::Mapping::Registry.new
        @map.set(StringArray, SOAP::SOAPArray,
                SOAP::Mapping::Registry::TypedArrayFactory,
                { :type => XSD::XSDString::Type })
        @drv.mapping_registry = @map
      end

      def get_bugs(*param)
        ### soap interface to get bugs with matching key/value.
        @drv.get_bugs(param)
      end

      def get_status(sa, ma_copies = nil)
        ### soap interface to get bug status matching the bug number(s). sa is an array of strings. ma_copies is the multiarch map.
        get_status = Debian::Bugs.new
        if sa.size == 0
          $stderr.puts "No bugs to fetch" if $DEBUG
        else
          $stderr.puts "fetching #{sa.join(" ")}.. " if $DEBUG
          # get the SOAP driver to send 'get_status'
          retval = @drv.get_status(sa)
          if ! retval.kind_of?(Hash)
            $stderr.puts "Unknown to the BTS: #{sa.join(" ")}" if $DEBUG
          else
            retval.each { |bugnum, res|
              # parse the received information, given from the server
              $stderr.puts res.inspect if $DEBUG
              if ma_copies.nil?
                packages = [res.package]
              else
                packages = res.package.split(/[ \t?,()]+/)
              end
              packages.each { |pkg_name|
                if ma_copies.nil?
                  keys = [pkg_name]
                else
                  keys = ma_copies[pkg_name]
                end
                if ( keys != nil )
                  keys.each { |pkg_key|
                    newbug = Debian::Bug.new(pkg_key,
                                             bugnum.to_s,
                                             res.severity,
                                             res.pending,
                                             res.subject.gsub(/\r/,''),
                                             res.tags.split(" "),
                                             res.mergedwith.to_s.split(" "),
                                             Time::at(res.date.to_i))
                    newbug.found =
                       res.found.keys.join(" ") if res.found.kind_of?(Hash)
                    newbug.fixed =
                       res.fixed.keys.join(" ") if res.fixed.kind_of?(Hash)
                    get_status << newbug
                  }
                end
              }
            }
          end
        end
        get_status
      end
    end
  end
end
