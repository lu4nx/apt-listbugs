#
# bts.rb - ruby interface for debian bts
# Copyright (C) 2002       Masato Taruishi <taru@debian.org>
# Copyright (C) 2006-2007  Junichi Uekawa <dancer@debian.org>
# Copyright (C) 2013-2022  Francesco Poli <invernomuto@paranoici.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License with
# the Debian GNU/Linux distribution in file /usr/share/common-licenses/GPL-2;
# if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Currently, this interface has only acquires to create bugs.

require 'aptlistbugs/debian/bug'
require 'uri.rb'
require 'find'
require 'zlib'
require 'thread'

module Debian
  module BTS
    class Parser

      def initialize(host, port)
        @host = host
        @port = port
      end

      # use SOAP interface to obtain the index.
      class SoapIndex < Parser
        def initialize(host, port)
          @host = host
          @port = port
          @indexes = {}
          @buf = nil
        end

        def parse_bug(bugnum)
          require 'aptlistbugs/debian/btssoap'
          soap = Debian::BTSSOAP::Soap.new(@host, @port)
          sa = Debian::BTSSOAP::StringArray.new

          # query the BTS about the given bug number
          sa << bugnum
          bugs = soap.get_status(sa)
          parse_bug = nil
          parse_bug = bugs.fetch(0) if bugs.length > 0
          parse_bug
        end

        def parse(ma_copies, querystep, parsestep, severities = ["critical", "grave"])
          require 'aptlistbugs/debian/btssoap'
          soap = Debian::BTSSOAP::Soap.new(@host, @port)
          sa = Debian::BTSSOAP::StringArray.new
          bugs = Debian::Bugs.new
          reqbugs = []

          # obtain a list of package names
          names = ma_copies.keys

          # query the BTS: ask for a list of bug numbers of given severities
          # affecting the given package names, in batches of querystep
          # packages
          names.each { |nm|
            sa << nm
            if sa.length >= querystep
              soap.get_bugs('severity', severities,
                             'package', sa).each { |b| reqbugs << b }
              sa = Debian::BTSSOAP::StringArray.new
            end
          }
          if sa.length != 0
            soap.get_bugs('severity', severities,
                           'package', sa).each { |b| reqbugs << b }
            sa = Debian::BTSSOAP::StringArray.new
          end

          # the total number of bugs
          max = reqbugs.length
          # progressed amount
          offset_i = 0

          # process each bug number and get contents, in batches of
          # parsestep bugs
          reqbugs.each { |bug|
            sa << bug
            if sa.length >= parsestep
              soap.get_status(sa, ma_copies).each { |b| bugs << b }
              offset_i += sa.length
              sa = Debian::BTSSOAP::StringArray.new
              pct = "#{((offset_i).to_f*100/max.to_f).to_i}%"
              pct = "#{pct}   ##{bug}" if parsestep == 1
              yield pct
            end
          }
          soap.get_status(sa, ma_copies).each { |b| bugs << b }
          bugs
        end
      end
    end
  end
end
